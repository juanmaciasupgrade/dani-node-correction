// importas express pero no se usa
const express = require('express');
const mongoose = require('mongoose');
const Hero = require('../model/Hero')
const fetch = require('node-fetch');

// Lo ideal sería importar aquí la URL de nuestro archivo db.js y solo tener la URL escrita en 1 lugar de nuestra app
DB_URL = 'mongodb://localhost:27017/Heroes';


mongoose.connect(DB_URL,{

    useNewUrlParser: true,
    useUnifiedTopology: true, 
})
.then ( ()=>{
    console.log("conectado")
    addmongo();
})
.catch (()=>{
    console.log("Error conecying ti DB");
});

// Nombre de función debe seguir camelCase. Nombre no descriptivo.
addmongo = async() =>{
    try{
        // variable no usada
        const array = [];
        
        // Esto se podría extraer a un objeto 
        const alterEgos = 'alter-egos';
        const placeOfBirth = 'place-of-birth';
        const firstAppearance ='first-appearance';
        const fullName = 'full-name';
        const hairColor = 'hair-color';
        const eyeColor = 'eye-color';
        const groupAffiliation = 'group-affiliation';


        // código mal indentado.
            // El máximo de ID (732) sería aconsejable guardarlo en una variable al principio del archivo.
            // este tipo de variable la nombraremos con mayusculas, algo así: const MAX_HEROES = 732;
            for (let id = 1; id <= 732; id++){
            //  mala indentación de nuevo
            const data = await fetch(`https://www.superheroapi.com/api.php/10217431065143700/${id}`);
       
            console.log("id: " + id)
            const hero = await data.json();
                
            //  desctructurar powerstats y biography de hero para no tener que repetirlo
            const newHero = new Hero(
             {
                id : Number(hero.id),
                name : hero.name,
                gender : hero.gender,
                image :{
                    url:hero.image.url
                },
                //  desctructurar powerstats, biography, appeareance, connections y work de hero para no tener que repetirlo
                powerStats:{
                    intelligence: Number(hero.powerstats.intelligence) ? hero.powerstats.intelligence : 1,
                    strength: Number(hero.powerstats.strength) ? hero.powerstats.strength : 1,
                    speed: Number(hero.powerstats.speed) ? hero.powerstats.speed : 1,
                    durability: Number(hero.powerstats.durability) ? hero.powerstats.durability : 1,
                    power: Number(hero.powerstats.power) ? hero.powerstats.power : 1,
                    combat: Number(hero.powerstats.combat) ? hero.powerstats.combat : 1,
                    
                },
                biography:{
                    fullName: hero.biography[fullName],
                    alterEgos: hero.biography[alterEgos],
                    aliases: hero.biography.aliases,
                    placeOfBirth: hero.biography[placeOfBirth],
                    firstAppearance: hero.biography[firstAppearance],
                    publisher: hero.biography.publisher,
                    alignment: hero.biography.alignment
                },
                work:{
                    base: hero.work.base,
                    occupation: hero.work.occupation
                },
                connections:{
                    groupAffiliation: hero.connections[groupAffiliation],
                    relatives: hero.connections.relatives
                },
                appearance:{
                    eyeColor: hero.appearance[eyeColor],
                    hairColor: hero.appearance[hairColor],
                    gender: hero.appearance.gender,
                    race: hero.appearance.race,
                    height: hero.appearance.height,
                    weight: hero.appearance.weight
                }
        } );
    
            const saveHero =  await newHero.save(newHero)
        
       }
    
        console.log('Scrap Finalizado')
    
    }catch (error){
        console.log("error downloading Api" + error)
    
    }
        
}


