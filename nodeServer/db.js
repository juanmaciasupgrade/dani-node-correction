const mongoose = require('mongoose');
const DB_URL = 'mongodb://localhost:27017/heroes';

//conexión a la db

// Recomendación: indentar bien el código
mongoose.connect(DB_URL, {
// Evitar líneas vacías (en esta línea no había nada)
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
// Esta línea sobra. Nunca se deja un espacio entre llamar a una promesa y la línea .then o .catch
.then ( ()=>{ // quitar espacios .then(() => ...)
    console.log("conectado")
})
.catch (()=>{ // .catch(() => ...)
    console.log("Error conecying ti DB");
})


// Falta la S a export: module.exportS
module.export = DB_URL;

